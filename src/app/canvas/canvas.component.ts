import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';
import {IMouse} from '../model/IMouse';
import {Tools} from '../model/Tools';
import {Layers} from '../model/Layers';

@Component({
    selector: 'app-canvas',
    templateUrl: './canvas.component.html',
    styleUrls: ['./canvas.component.scss'],
})
export class CanvasComponent implements AfterViewInit {
    @ViewChild('myCanvas', {static: false}) myCanvas: ElementRef;
    public context: CanvasRenderingContext2D | null;
    public mouse: IMouse;
    public layers: any[] = [];
    public config = {
        width: 800,
        height: 600
    };
    public selectedTool: Tools | null = null;
    public selectedLayer: any = null;
    public newlyCreatedLayer: any = null;
    public availableTools = [
        Tools.DRAW_LINE,
        Tools.DRAW_TRIANGLE,
        Tools.DRAW_RECT,
        Tools.DRAW_ELLIPSE,
        Tools.DRAW_FREE
    ];

    constructor() {
    }

    ngAfterViewInit(): void {
        this.context = this.myCanvas.nativeElement.getContext('2d');
        if (this.context) {
            this.context.canvas.width = this.config.width;
            this.context.canvas.height = this.config.height;
        }
        this.clearCanvas();
        this.mouse = {
            xDown: 0, xUp: 0, yDown: 0, yUp: 0, mouseDown: false
        };
    }

    mouseDown(e: MouseEvent): void {
        this.mouse.xDown = e.clientX;
        this.mouse.yDown = e.clientY;
        this.mouse.mouseDown = true;
        if (this.selectedTool && !this.selectedLayer ) {
            this.addLayer();
        }
    }

    mouseUp(e: MouseEvent): void {
        this.mouse.mouseDown = false;
        this.mouse.xUp = e.clientX;
        this.mouse.yUp = e.clientY;
        this.newlyCreatedLayer = null;
        if (this.selectedLayer ) {
            this.updateSelectedLayerPositionEnd(e);
        }
    }

    mouseMove(e: MouseEvent): void {
        if ( this.mouse.mouseDown && !this.selectedLayer ) { this.updateSelectedLayerEndPosition(e); }
        if ( this.mouse.mouseDown && this.selectedLayer ) { this.updateSelectedLayerPosition(e); }
        this.refresh();
    }
    private updateSelectedLayerPosition(e: MouseEvent): void {
        const vRelative = {
            x: e.clientX - this.mouse.xDown,
            y: e.clientY - this.mouse.yDown
        };
        this.selectedLayer.offsetX = vRelative.x;
        this.selectedLayer.offsetY = vRelative.y;
    }
    private updateSelectedLayerPositionEnd(e: MouseEvent): void {
        const vRelative = {
            x: e.clientX - this.mouse.xDown,
            y: e.clientY - this.mouse.yDown
        };

        if ( this.selectedLayer.type === Layers.FREE_DRAW ) {
            this.selectedLayer.pathPoints.forEach((point: {x: number, y: number}): void => {
                point.x = point.x + vRelative.x;
                point.y = point.y + vRelative.y;
            });
        } else {
            this.selectedLayer.object.startX = this.selectedLayer.object.startX + vRelative.x;
            this.selectedLayer.object.startY = this.selectedLayer.object.startY + vRelative.y;
            this.selectedLayer.object.endX = this.selectedLayer.object.endX + vRelative.x;
            this.selectedLayer.object.endY = this.selectedLayer.object.endY + vRelative.y;
        }
        this.selectedLayer.offsetX = 0;
        this.selectedLayer.offsetY = 0;
    }
    private updateSelectedLayerEndPosition(e: MouseEvent): void {
        if ( this.newlyCreatedLayer.type === Layers.FREE_DRAW ) {
            this.newlyCreatedLayer.pathPoints.push({x: e.clientX, y: e.clientY});
        } else {
            this.newlyCreatedLayer.object.endX = e.clientX;
            this.newlyCreatedLayer.object.endY = e.clientY;
        }
    }

    private addLayer(): void {
        switch (this.selectedTool) {
            case Tools.DRAW_LINE:
                this.addLineLayer();
                break;
            case Tools.DRAW_TRIANGLE:
                this.addTriangleLayer();
                break;
            case Tools.DRAW_RECT:
                this.addRectLayer();
                break;
            case Tools.DRAW_ELLIPSE:
                this.addEllipseLayer();
                break;
            case Tools.DRAW_FREE:
                this.addFreeDrawLayer();
                break;
            default:
                break;
        }
        this.newlyCreatedLayer = this.layers[this.layers.length - 1];
    }

    private addLineLayer(): void {
        this.layers.push({
            type: Layers.LINE,
            preview: true,
            object: {
                startX: this.mouse.xDown,
                startY: this.mouse.yDown,
                endX: this.mouse.xDown,
                endY: this.mouse.yDown,
            },
            offsetX: 0,
            offsetY: 0,
            scale: 1
        });
    }
    private addTriangleLayer(): void {
        this.layers.push({
            type: Layers.TRIANGLE,
            preview: true,
            object: {
                startX: this.mouse.xDown,
                startY: this.mouse.yDown,
                endX: this.mouse.xDown,
                endY: this.mouse.yDown,
            },
            offsetX: 0,
            offsetY: 0,
            scale: 1
        });
    }
    private addRectLayer(): void {
        this.layers.push({
            type: Layers.RECT,
            preview: true,
            object: {
                startX: this.mouse.xDown,
                startY: this.mouse.yDown,
                endX: this.mouse.xDown,
                endY: this.mouse.yDown,
            },
            offsetX: 0,
            offsetY: 0,
            scale: 1
        });
    }
    private addEllipseLayer(): void {
        this.layers.push({
            type: Layers.ELLIPSE,
            preview: true,
            object: {
                startX: this.mouse.xDown,
                startY: this.mouse.yDown,
                endX: this.mouse.xDown,
                endY: this.mouse.yDown,
            },
            offsetX: 0,
            offsetY: 0,
            scale: 1
        });
    }
    private addFreeDrawLayer(): void {
        this.layers.push({
            type: Layers.FREE_DRAW,
            preview: true,
            pathPoints: [
                { x: this.mouse.xDown, y: this.mouse.yDown }
            ],
            offsetX: 0,
            offsetY: 0,
            scale: 1
        });
    }

    private clearCanvas(): void {
        if (this.context) {
            this.context.fillStyle = '#e0e0e0';
            this.context.fillRect(0, 0, this.config.width, this.config.height);
        }
    }

    private printMousePosition(e: MouseEvent): void {
        if (this.context) {
            this.context.fillStyle = '#000000';
            this.context.fillText('X: ' + e.clientX + ' Y: ' + e.clientY, 10, 10);
        }
    }

    private refresh(): void {
        if (this.context) {
            this.clearCanvas();
            this.drawLayers();
        }
    }
// methods for drawing new layers
    private drawLayers(): void {
        this.layers.forEach(layer => {
            switch (layer.type){
                case Layers.LINE:
                    this.drawLineLayer(layer);
                    break;
                case Layers.TRIANGLE:
                    this.drawTriangleLayer(layer);
                    break;
                case Layers.RECT:
                    this.drawRectLayer(layer);
                    break;
                case Layers.ELLIPSE:
                    this.drawEllipseLayer(layer);
                    break;
                case Layers.FREE_DRAW:
                    this.drawFreeDrawLayer(layer);
                    break;
                default:
                    break;
            }
        });
    }

    private drawLineLayer(layer: any): void {
        if (this.context) {
            this.context.fillStyle = '#000000';
            this.context.beginPath();
            this.context.moveTo(layer.object.startX + layer.offsetX, layer.object.startY + layer.offsetY);
            this.context.lineTo(layer.object.endX + layer.offsetX, layer.object.endY + layer.offsetY);
            this.context.stroke();
        }
    }
    private drawRectLayer(layer: any): void {
        if (this.context) {
            const rect = {
                x: layer.object.startX < layer.object.endX ? layer.object.startX : layer.object.endX,
                y: layer.object.startY < layer.object.endY ? layer.object.startY : layer.object.endY,
                width: layer.object.startX - layer.object.endX >= 0 ? layer.object.startX - layer.object.endX : layer.object.endX - layer.object.startX,
                height: layer.object.startY - layer.object.endY >= 0 ? layer.object.startY - layer.object.endY : layer.object.endY - layer.object.startY,
            };
            this.context.fillStyle = '#000000';
            this.context.beginPath();
            this.context.rect(rect.x + layer.offsetX, rect.y + layer.offsetY, rect.width * layer.scale, rect.height * layer.scale);
            this.context.stroke();
        }
    }
    private drawEllipseLayer(layer: any): void {
        if (this.context) {
            const elipse = {
                x: layer.object.startX < layer.object.endX ? layer.object.startX : layer.object.endX,
                y: layer.object.startY < layer.object.endY ? layer.object.startY : layer.object.endY,
                width: layer.object.startX - layer.object.endX >= 0 ? layer.object.startX - layer.object.endX : layer.object.endX - layer.object.startX,
                height: layer.object.startY - layer.object.endY >= 0 ? layer.object.startY - layer.object.endY : layer.object.endY - layer.object.startY,
            };
            this.context.fillStyle = '#000000';
            this.context.beginPath();
            this.context.ellipse(elipse.x + layer.offsetX, elipse.y + layer.offsetY, elipse.width * layer.scale, elipse.height * layer.scale, 0, 0, 2 * Math.PI);
            this.context.stroke();
        }
    }
    private drawFreeDrawLayer(layer: any): void {
        if (this.context) {
            this.context.fillStyle = '#000000';
            for (let i = 0; i < layer.pathPoints.length - 1; i++) {
                this.context.beginPath();
                this.context.moveTo(layer.pathPoints[i].x + layer.offsetX, layer.pathPoints[i].y + layer.offsetY);
                this.context.lineTo(layer.pathPoints[i + 1].x + layer.offsetX, layer.pathPoints[i + 1].y + layer.offsetY);
                this.context.stroke();
            }
        }
    }
    private drawTriangleLayer(layer: any): void {
        if (this.context) {
            const point = { x: layer.object.startX, y: layer.object.endY };
            this.context.fillStyle = '#000000';
            this.context.beginPath();
            this.context.moveTo(layer.object.startX + layer.offsetX, layer.object.startY + layer.offsetY);
            this.context.lineTo(point.x + layer.offsetX, point.y + layer.offsetY);
            this.context.lineTo(layer.object.endX + layer.offsetX, layer.object.endY + layer.offsetY);
            this.context.closePath();
            this.context.stroke();
        }
    }


    public changeTool(tool: Tools): void {
        this.selectedTool = tool;
        this.selectedLayer = null;
    }

    public toggleSelectedLayer(i: number): void {
        if (this.selectedLayer === this.layers[i]) { this.selectedLayer = null; }
        else { this.selectedLayer = this.layers[i]; }
        this.selectedTool = null;
    }

    public scaleUp(): void {
        this.selectedLayer.scale += 0.1;
        this.refresh();
    }

    public scaleDown(): void {
        this.selectedLayer.scale -= 0.1;
        this.refresh();
    }
    public saveImage(): void {
        const link = document.getElementById('link');
        if (link){
            link.setAttribute('download', 'image.png');
            link.setAttribute('href', this.myCanvas.nativeElement.toDataURL('image/png').replace('image/png', 'image/octet-stream'));
            link.click();
        }
    }
    public ableToScale(): boolean {
        if (this.selectedLayer){
            return [Layers.ELLIPSE, Layers.RECT].includes(this.selectedLayer.type);
        } else { return false; }
    }
}
