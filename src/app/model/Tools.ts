export enum Tools {
    DRAW_LINE = 'draw line',
    DRAW_TRIANGLE = 'draw triangle',
    DRAW_RECT = 'draw rect',
    DRAW_ELLIPSE = 'draw ellipse',
    DRAW_FREE = 'free draw',
    SCALE_UP = 'scale up',
    SCALE_DOWN = 'scale down'
}
