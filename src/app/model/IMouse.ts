export interface IMouse{
  xUp: number;
  yUp: number;
  xDown: number;
  yDown: number;
  mouseDown: boolean;
}
