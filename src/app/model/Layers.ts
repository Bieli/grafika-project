export enum Layers {
    LINE = 'Line layer',
    TRIANGLE = 'Triangle layer',
    RECT = 'Rectangle layer',
    ELLIPSE = 'Ellipse layer',
    FREE_DRAW = 'Free draw layer'
}
